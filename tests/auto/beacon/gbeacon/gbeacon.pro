CONFIG += testcase parallel_test
TARGET = tst_gbeacon
osx:CONFIG -= app_bundle

QT += beacon beacon-private testlib
SOURCES += \
    tst_gbeacon.cpp
