#include <QtQml/qqmlextensionplugin.h>
#include <QtQml/qqml.h>

#include <GroupsIncBeacon/gbeaconmodel.h>
#include <GroupsIncBeacon/private/gbeaconregion_p.h>
#include <GroupsIncBeacon/private/gbeaconbeacon_p.h>
#include <GroupsIncMvc/gmvcfacade.h>

QT_BEGIN_NAMESPACE

GMVC_DEFINE_MODEL(BeaconModel)

class GBeaconModule : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface/1.0")
public:
    void registerTypes(const char *uri)
    {
        Q_ASSERT(QLatin1String(uri) == QLatin1String("GroupsIncBeacon"));

        // @uri GroupsIncBeacon
        qmlRegisterSingletonType<BeaconModel>(uri, 1, 0, "BeaconModel", GMVC_MODEL(BeaconModel));
        qmlRegisterType<Beacon>(uri, 1, 0, "Beacon");
        qmlRegisterType<Region>(uri, 1, 0, "Region");
    }

    void initializeEngine(QQmlEngine *engine, const char *uri)
    {
        Q_UNUSED(uri);
        Q_UNUSED(engine);
    }
};

QT_END_NAMESPACE

#include "beacon.moc"



