CXX_MODULE = beacon
TARGET  = declarative_beacon
TARGETPATH = GroupsIncBeacon
IMPORT_VERSION = 1.0

QT += qml quick beacon beacon-private
SOURCES += \
    $$PWD/beacon.cpp

load(qml_plugin)

OTHER_FILES += qmldir
