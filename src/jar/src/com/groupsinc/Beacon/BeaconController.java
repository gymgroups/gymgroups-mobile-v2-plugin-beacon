package com.groupsinc.Beacon;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.util.Log;
import org.altbeacon.beacon.*;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.service.RangedBeacon;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

import java.util.Collection;

@SuppressWarnings("unused")
public class BeaconController implements BeaconConsumer, BootstrapNotifier, RangeNotifier
{
	private static final String TAG = "BeaconController";

    private final int ERROR_NOT_READY = 1;
    private final int ERROR_BLUETOOTH_NOT_ENABLED = 2;
    private final int ERROR_BLE_NOT_SUPPORTED = 3;
    private final int ERROR_MISSING_REGION = 4;

    private Activity m_activity = null;

    private BeaconManager m_beaconManager;
    private Region m_region;

    @SuppressWarnings("unused")
    private BackgroundPowerSaver m_backgroundPowerSaver;

    @SuppressWarnings("unused")
    private RegionBootstrap m_regionBootstrap;

    private boolean m_inited = false;

    private native void c_onServiceConnected();
    private native void c_onExitRegion(Region region);
    private native void c_onEnterRegion(Region region);
    private native void c_onError(int error);
    private native void c_onBeaconDetectedInRange(Beacon[] beacons, Region region);

    public BeaconController(final Activity activity)
    {
        m_activity = activity;
        m_beaconManager = BeaconManager.getInstanceForApplication(m_activity);
    }
	
	public void setBackgroundMode(boolean backgroundMode)
	{
		m_beaconManager.setBackgroundMode(backgroundMode);
	}

    public void useBackgroundPowerSaver()
    {
        if (m_backgroundPowerSaver != null)
            m_backgroundPowerSaver = new BackgroundPowerSaver(m_activity);
    }

    public void setBackgroundBetweenScanPeriod(long period)
    {
        m_beaconManager.setBackgroundBetweenScanPeriod(period);
    }
	
	public void setForegroundBetweenScanPeriod(long period)
	{
        m_beaconManager.setForegroundBetweenScanPeriod(period);
	}

    public void setBackgroundScanPeriod(long period)
    {
        m_beaconManager.setBackgroundScanPeriod(period);
    }

    public void setForegroundScanPeriod(long period)
    {
        m_beaconManager.setForegroundScanPeriod(period);
    }

    public void appendLayout(String layout)
    {
        m_beaconManager
                .getBeaconParsers()
                .add(new BeaconParser().setBeaconLayout(layout));
    }

    public void setLayout(String layout)
    {
        clearLayouts();
        m_beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(layout));
    }

    public void clearLayouts()
    {
        m_beaconManager.getBeaconParsers().clear();
    }

    public void setSampleExpiration(long millisecond)
    {
        RangedBeacon.setSampleExpirationMilliseconds(millisecond);
    }

    public void setRegion(String uniqueId, String id1, int id2, int id3)
    {
        setRegion(uniqueId, Identifier.parse(id1), Identifier.fromInt(id2), Identifier.fromInt(id3));
    }

    public void setRegion(String uniqueId, String id1)
    {
        setRegion(uniqueId, Identifier.parse(id1), null, null);
    }

    public void setRegion(String uniqueId)
    {
        setRegion(uniqueId, null, null, null);
    }

    public void setRegion(String uniqueId, Identifier id1, Identifier id2, Identifier id3)
    {
        m_region = new Region(uniqueId, id1, id2, id3);
        if (m_regionBootstrap != null) {
            m_regionBootstrap.disable();
            m_regionBootstrap = new RegionBootstrap(this, m_region);
        }
    }

    public boolean isValid()
    {
        return isValid(false);
    }

    public boolean isValid(boolean notifyErrors)
    {
        try {
            if (!m_beaconManager.checkAvailability()) {
                if (notifyErrors)
                    c_onError(ERROR_BLUETOOTH_NOT_ENABLED);
                return false;
            }

        } catch (BleNotAvailableException e) {
            if (notifyErrors)
                c_onError(ERROR_BLE_NOT_SUPPORTED);
            return false;
        }

        return true;
    }

    private boolean checkReady()
    {
        if (!m_inited) {
            c_onError(ERROR_NOT_READY);
            return false;
        }

        return true;
    }

    public void init()
    {
        if (m_inited || !isValid(true))
            return;

        if (m_region == null) {
            c_onError(ERROR_MISSING_REGION);
            return;
        }

        m_regionBootstrap = new RegionBootstrap(this, m_region);
        m_beaconManager.setMonitorNotifier(this);
        m_beaconManager.setRangeNotifier(this);

        m_inited = true;
    }

    public void startMonitoringBeaconsInRegion()
    {
        if (!checkReady())
            return;

        try {
            m_beaconManager.startMonitoringBeaconsInRegion(m_region);
        } catch (RemoteException e) {
            c_onError(ERROR_NOT_READY);
            e.printStackTrace();
        }
    }

    public void stopMonitoringBeaconsInRegion()
    {
        if (!checkReady())
            return;

        try {
            m_beaconManager.stopMonitoringBeaconsInRegion(m_region);
        } catch (RemoteException e) {
            c_onError(ERROR_NOT_READY);
            e.printStackTrace();
        }
    }

    public void startRangingBeaconsInRegion()
    {
        if (!checkReady())
            return;

        try {
            m_beaconManager.startRangingBeaconsInRegion(m_region);
        } catch (RemoteException e) {
            c_onError(ERROR_NOT_READY);
            e.printStackTrace();
        }
    }

    public void stopRangingBeaconsInRegion()
    {
        if (!checkReady())
            return;

        try {
            m_beaconManager.stopRangingBeaconsInRegion(m_region);
        } catch (RemoteException e) {
            c_onError(ERROR_NOT_READY);
            e.printStackTrace();
        }
    }

    @Override
    public void onBeaconServiceConnect() {
        Log.d(TAG, "Beacon service connected.");
        c_onServiceConnected();
    }

    @Override
    public void didEnterRegion(Region region) {
        Log.d(TAG, "Received: didEnterRegion");
        c_onEnterRegion(region);
    }

    @Override
    public void didExitRegion(Region region) {
        Log.d(TAG, "Received: didExitRegion");
        c_onExitRegion(region);
    }

    @Override
    public void didDetermineStateForRegion(int i, Region region) {
        Log.d(TAG, "Received: didDetermineStateForRegion: " + i);
    }

    @Override
    public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
        for (Beacon beacon: beacons)
            Log.d(TAG, "Beacon Received: " + beacon.toString() + " Distance: " + beacon.getDistance());

        Beacon[] arrayList = beacons.toArray(new Beacon[beacons.size()]);
        c_onBeaconDetectedInRange(arrayList, region);
    }

    @Override
    public Context getApplicationContext() {
        return m_activity.getApplicationContext();
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {
        Log.d(TAG, "Received unbindService request which is odd.");
    }

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
        Log.d(TAG, "Received bindService request which is odd.");
        return false;
    }
}
