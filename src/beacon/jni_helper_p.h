#ifndef JNI_HELPER_H
#define JNI_HELPER_H

#include <QtAndroidExtras>
#include "gbeaconmodel.h"

static inline QString toQString(JNIEnv *env, jstring value)
{
    return QString::fromUtf8(env->GetStringUTFChars(value, 0));
}

static inline void beaconDetectionStarted(JNIEnv *, jclass)
{
    // emit BeaconManager::instance()->beaconDetectionStarted();
}

static inline void beaconDetected(JNIEnv *env, jclass, jstring id1, jstring id2, jstring id3, jstring address, jdouble distance)
{
    /*qDebug() << "Beacon detected {" << id1 << id2 << id3 << address << distance << "}";
    emit BeaconManager::instance()->beaconReceived(
                toQString(env, id1),
                toQString(env, id2),
                toQString(env, id3),
                toQString(env, address),
                static_cast<qreal>(distance));*/
}

static inline void beaconDetectionFinished(JNIEnv *, jclass)
{
    // emit BeaconManager::instance()->beaconDetectionFinished();
}

static inline void stateChanged(JNIEnv *env, jclass, jstring state)
{
   //  emit BeaconManager::instance()->stateChanged(toQString(env, state));
}

JNINativeMethod jniMethods[] = {
    { "beaconDetectionStarted", "()V", (void *) beaconDetectionStarted },
    { "beaconDetected", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V", (void *) beaconDetected },
    { "beaconDetectionFinished", "()V", (void *) beaconDetectionFinished },
    { "stateChanged", "(Ljava/lang/String;)V", (void *) stateChanged }
};

int JNICALL JNI_OnLoad(JavaVM *vm, void *)
{
    JNIEnv *env;
    if (vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_4) != JNI_OK)
        return JNI_FALSE;

    jclass mainActivity = env->FindClass("com/ble/MainActivity");
    if (!mainActivity)
        return JNI_ERR;

    if (env->RegisterNatives(mainActivity, jniMethods, sizeof(jniMethods) / sizeof(jniMethods[0])))
        return JNI_ERR;

    return JNI_VERSION_1_4;
}

#endif // JNI_HELPER_H
