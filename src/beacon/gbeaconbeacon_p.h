#ifndef GBEACONBEACON_P_H
#define GBEACONBEACON_P_H

#include <QObject>

class Beacon;

class BeaconPrivate : public QObject
{
    Q_OBJECT
public:
    explicit BeaconPrivate(QObject *parent = 0)
        : QObject(parent)
        , m_major(-1)
        , m_minor(-1)
        , m_rssi(0)
        , m_txPower(0)
        , m_distance(0.) {}

private:
    QString m_uuid;
    QString m_address;
    qint32 m_major;
    qint32 m_minor;
    qint32 m_rssi;
    qint32 m_txPower;
    qreal m_distance;

    friend class Beacon;
};

#endif // GBEACONBEACON_P_H
