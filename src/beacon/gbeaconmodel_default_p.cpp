#include "gbeaconmodel_default_p.h"

BeaconModelPrivate *BeaconModelPrivate::create(BeaconModel *q)
{
    return new BeaconModelDefaultPrivate(q);
}

BeaconModelDefaultPrivate::BeaconModelDefaultPrivate(BeaconModel *q)
    : BeaconModelPrivate(q)
{
}

void BeaconModelDefaultPrivate::startMonitoringBeaconsInRegion()
{
}

void BeaconModelDefaultPrivate::stopMonitoringBeaconsInRegion()
{
}

void BeaconModelDefaultPrivate::startRangingBeaconsInRegion()
{
}

void BeaconModelDefaultPrivate::stopRangingBeaconsInRegion()
{
}

bool BeaconModelDefaultPrivate::isInProgress() const
{
    return false;
}

void BeaconModelDefaultPrivate::init()
{
}
