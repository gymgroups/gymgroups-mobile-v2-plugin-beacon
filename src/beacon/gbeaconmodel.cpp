#include "gbeaconmodel.h"
#include <QDebug>
#include <QRegExp>
#include "gbeaconmodel_p.h"
#include "gbeaconbeacon_p.h"
#include "gbeaconregion_p.h"

const char *BeaconModel::NAME = "BeaconModel";

BeaconModel::BeaconModel(MvcFacade *parent)
    : MvcModel(parent)
    , d_ptr(BeaconModelPrivate::create(this))
{
}

void BeaconModel::appendLayout(const QString &layout)
{
    Q_D(BeaconModel);
    d->m_layouts.append(layout);
}

void BeaconModel::setLayout(const QString &layout)
{
    Q_D(BeaconModel);
    d->m_layouts.clear();
    d->m_layouts.append(layout);
}

void BeaconModel::clearLayout()
{
    Q_D(BeaconModel);
    d->m_layouts.clear();
}

QList<QString> BeaconModel::layouts() const
{
    Q_D(const BeaconModel);
    return d->m_layouts;
}

bool BeaconModel::hasRegion(const QString &uuid, qint32 major, qint32 minor)
{
    Q_D(const BeaconModel);
    int i = 0;
    int len = d->m_regions.count();

    Region *region = 0;

    for (; i < len; i++) {
        region = d->m_regions.at(i);
        if (region->uuid() == uuid
                && region->major() == major
                && region->minor() == minor) {
            return true;
        }
    }

    return false;
}

bool BeaconModel::addRegion(const QString &uuid, qint32 major, qint32 minor)
{
    Q_D(BeaconModel);
    bool success = true;

    if (!hasRegion(uuid, major, minor)) {
        Region *region = new Region(this);
        region->setUuid(uuid);
        region->setMajor(major);
        region->setMinor(minor);

        if (!region->isValid()) {
            delete region;
            region = 0;

            success = false;
        } else {
            d->m_regions.append(region);
        }
    }

    return success;
}

bool BeaconModel::removeRegion(const QString &uuid, qint32 major, qint32 minor)
{
    Q_D(BeaconModel);
    int i = 0;
    int len = d->m_regions.count();

    Region *region = 0;

    for (; i < len; i++) {
        region = d->m_regions.at(i);
        if (region->uuid() == uuid
                && region->major() == major
                && region->minor() == minor) {
            d->m_regions.removeAt(i);
            return true;
        }
    }

    return false;
}

void BeaconModel::startMonitoringBeaconsInRegion()
{
    Q_D(BeaconModel);
    d->startMonitoringBeaconsInRegion();
}

void BeaconModel::stopMonitoringBeaconsInRegion()
{
    Q_D(BeaconModel);
    d->stopMonitoringBeaconsInRegion();
}

void BeaconModel::startRangingBeaconsInRegion()
{
    Q_D(BeaconModel);
    d->startRangingBeaconsInRegion();
}

void BeaconModel::stopRangingBeaconsInRegion()
{
    Q_D(BeaconModel);
    d->stopRangingBeaconsInRegion();
}

bool BeaconModel::isInProgress() const
{
    Q_D(const BeaconModel);
    return d->isInProgress();
}

int BeaconModel::error() const
{
    return 0;
}

void BeaconModel::setError(int error)
{
    Q_UNUSED(error);
}

void BeaconModel::init()
{
    Q_D(BeaconModel);
    d->init();
}

const char *BeaconModel::name()
{
    return NAME;
}

void BeaconModel::apply(const QVariantMap &config)
{
    Q_UNUSED(config);
}

// Region
Region::Region(QObject *parent)
    : QObject(parent)
    , d_ptr(new RegionPrivate())
{
}

Region::~Region()
{
    delete d_ptr;
}

bool Region::isValid() const
{
    Q_D(const Region);
    if (d->m_uuid.isEmpty())
        return false;

    QRegExp regex("^[a-zA-Z0-9\\-]{36}$");
    if (regex.indexIn(d->m_uuid) == -1)
        return false;

    return true;
}

void Region::setUuid(const QString &uuid)
{
    Q_D(Region);
    if (d->m_uuid != uuid) {
        d->m_uuid = uuid;
        emit uuidChanged();
    }
}

QString Region::uuid() const
{
    Q_D(const Region);
    return d->m_uuid;
}

void Region::setMajor(qint32 major)
{
    Q_D(Region);
    if (d->m_major != major) {
        d->m_major = major;
        emit majorChanged();
    }
}

qint32 Region::major() const
{
    Q_D(const Region);
    return d->m_major;
}

void Region::setMinor(qint32 minor)
{
    Q_D(Region);
    if (d->m_minor != minor) {
        d->m_minor = minor;
        emit minorChanged();
    }
}

qint32 Region::minor() const
{
    Q_D(const Region);
    return d->m_minor;
}

// Beacon
Beacon::Beacon(QObject *parent)
    : QObject(parent)
    , d_ptr(new BeaconPrivate())
{
}

Beacon::~Beacon()
{
    delete d_ptr;
}

void Beacon::setRssi(qint32 rssi)
{
    Q_D(Beacon);
    if (d->m_rssi != rssi) {
        d->m_rssi = rssi;
        emit rssiChanged();
    }
}

qint32 Beacon::rssi() const
{
    Q_D(const Beacon);
    return d->m_rssi;
}

void Beacon::setTxPower(qint32 txPower)
{
    Q_D(Beacon);
    if (d->m_txPower != txPower) {
        d->m_txPower = txPower;
        emit txPowerChanged();
    }
}

qint32 Beacon::txPower() const
{
    Q_D(const Beacon);
    return d->m_txPower;
}

void Beacon::setUuid(const QString &uuid)
{
    Q_D(Beacon);
    if (d->m_uuid != uuid) {
        d->m_uuid = uuid;
        emit uuidChanged();
    }
}

QString Beacon::uuid() const
{
    Q_D(const Beacon);
    return d->m_uuid;
}

void Beacon::setMajor(qint32 major)
{
    Q_D(Beacon);
    if (d->m_major != major) {
        d->m_major = major;
        emit majorChanged();
    }
}

qint32 Beacon::major() const
{
    Q_D(const Beacon);
    return d->m_major;
}

void Beacon::setMinor(qint32 minor)
{
    Q_D(Beacon);
    if (d->m_minor != minor) {
        d->m_minor = minor;
        emit minorChanged();
    }
}

qint32 Beacon::minor() const
{
    Q_D(const Beacon);
    return d->m_minor;
}

void Beacon::setDistance(qreal distance)
{
    Q_D(Beacon);
    if (d->m_distance != distance) {
        d->m_distance = distance;
        emit distanceChanged();
    }
}

qreal Beacon::distance() const
{
    Q_D(const Beacon);
    return d->m_distance;
}
