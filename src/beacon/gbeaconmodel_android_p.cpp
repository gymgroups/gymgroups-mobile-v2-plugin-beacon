#include "gbeaconmodel_android_p.h"
#include "jni_helper_p.h"

BeaconModelPrivate *BeaconModelPrivate::create(BeaconModel *q)
{
    return new BeaconModelAndroidPrivate(q);
}

BeaconModelAndroidPrivate::BeaconModelAndroidPrivate(BeaconModel *q)
    : BeaconModelPrivate(q)
{
}

void BeaconModelAndroidPrivate::startMonitoringBeaconsInRegion()
{

}

void BeaconModelAndroidPrivate::stopMonitoringBeaconsInRegion()
{

}

void BeaconModelAndroidPrivate::startRangingBeaconsInRegion()
{

}

void BeaconModelAndroidPrivate::stopRangingBeaconsInRegion()
{

}

bool BeaconModelAndroidPrivate::isInProgress() const
{
    return false;
}

void BeaconModelAndroidPrivate::init()
{
}
