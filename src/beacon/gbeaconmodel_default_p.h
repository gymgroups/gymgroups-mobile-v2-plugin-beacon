#ifndef BEACONMODELDEFAULT_H
#define BEACONMODELDEFAULT_H

#include <QObject>
#include "gbeaconmodel_p.h"

class BeaconModelDefaultPrivate : public BeaconModelPrivate
{
    Q_OBJECT
public:
    BeaconModelDefaultPrivate(BeaconModel *q);

    void startMonitoringBeaconsInRegion();
    void stopMonitoringBeaconsInRegion();

    void startRangingBeaconsInRegion();
    void stopRangingBeaconsInRegion();

    bool isInProgress() const;

    void init();
};

#endif // BEACONMODELDEFAULT_H
