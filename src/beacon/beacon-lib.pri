ANDROID_BUNDLED_JAR_DEPENDENCIES = \
    jar/GroupsIncAndroidBeacon-bundled.jar
ANDROID_JAR_DEPENDENCIES = \
    jar/GroupsIncAndroidBeacon.jar

INCLUDEPATH += $$PWD

PUBLIC_HEADERS += \
    gbeacon_global.h \
    gbeaconmodel.h

PRIVATE_HEADERS += \
    gbeaconmodel_p.h \
    gbeaconregion_p.h \
    gbeaconbeacon_p.h

SOURCES += \
    gbeaconmodel.cpp

android {
    QT += androidextras

    PRIVATE_HEADERS += \
        gbeaconmodel_android_p.h \
        jni_helper_p.h

    SOURCES += \
        gbeaconmodel_android_p.cpp

} else:ios {

    PRIVATE_HEADERS += \
        gbeaconmodel_ios_p.h

    OBJECTIVE_SOURCES += \
        gbeaconmodel_ios_p.mm

} else {

    PRIVATE_HEADERS += \
        gbeaconmodel_default_p.h

    SOURCES += \
        gbeaconmodel_default_p.cpp
}

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS
