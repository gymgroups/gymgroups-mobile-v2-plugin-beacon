#ifndef BEACONMODELANDROID_H
#define BEACONMODELANDROID_H

#include "gbeaconmodel_p.h"
#include "gbeaconmodel.h"
#include <QDebug>

class BeaconModelAndroidPrivate : public BeaconModelPrivate
{
    Q_OBJECT
public:
    BeaconModelAndroidPrivate(BeaconModel *q);

    void startMonitoringBeaconsInRegion();
    void stopMonitoringBeaconsInRegion();

    void startRangingBeaconsInRegion();
    void stopRangingBeaconsInRegion();

    bool isInProgress() const;

    void init();
};

#endif // BEACONMODELANDROID_H
