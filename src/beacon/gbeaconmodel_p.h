#ifndef BEACONMODELPRIVATE_H
#define BEACONMODELPRIVATE_H

#include <QObject>
#include "gbeaconmodel.h"

class Region;

class BeaconModelPrivate : public QObject
{
    Q_OBJECT
public:
    BeaconModelPrivate(BeaconModel *q)
        : QObject(0)
        , q_ptr(q)
        , m_backgroundMode(false)
        , m_powerSaver(true) {}

    virtual ~BeaconModelPrivate() {}

    static BeaconModelPrivate *create(BeaconModel *q);

    /* For v2
    virtual void setBackgroundMode(bool backgroundMode) = 0;
    virtual bool backgroundMode() const = 0;

    virtual void setBackgroundPowerSaver(bool backgroundPowerSaver) = 0;
    virtual bool backgroundPowerSaver() const = 0;

    virtual void setBackgroundBetweenScanPeriod(qint64 scanPeriod) = 0;
    virtual void setBackgroundScanPeriod(qint64 scanPeriod) = 0;
    virtual void setForegroundBetweenScanPeriod(qint64 scanPeriod) = 0;
    virtual void setForegroundScanPeriod(qint64 scanPeriod) = 0;
    virtual void setSampleExpiration(qint64 milliseconds) = 0;

    virtual bool isValid() = 0;
    virtual void init() = 0;
    */

    virtual void startMonitoringBeaconsInRegion() = 0;
    virtual void stopMonitoringBeaconsInRegion() = 0;

    virtual void startRangingBeaconsInRegion() = 0;
    virtual void stopRangingBeaconsInRegion() = 0;

    virtual bool isInProgress() const = 0;

    virtual void init() = 0;

    BeaconModel *q() { return q_ptr; }
    QList<Region *> regions() { return m_regions; }

protected:
    Q_DECLARE_PUBLIC(BeaconModel)
    BeaconModel *q_ptr;

    bool m_backgroundMode;
    bool m_powerSaver;
    QList<QString> m_layouts;
    QList<Region *> m_regions;
};

#endif // BEACONMODELPRIVATE_H
