#ifndef BEACON_GLOBAL_H
#define BEACON_GLOBAL_H

#include <QtCore/qglobal.h>

QT_BEGIN_NAMESPACE

#ifndef QT_STATIC
#  if defined(QT_BUILD_BEACON_LIB)
#    define Q_BEACON_EXPORT Q_DECL_EXPORT
#  else
#    define Q_BEACON_EXPORT Q_DECL_IMPORT
#  endif
#else
#  define Q_BEACON_EXPORT
#endif

QT_END_NAMESPACE

#endif
