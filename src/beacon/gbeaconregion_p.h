#ifndef GBEACONREGION_P_H
#define GBEACONREGION_P_H

#include <QObject>

class Region;

class RegionPrivate : public QObject
{
    Q_OBJECT
public:
    explicit inline RegionPrivate(QObject *parent = 0)
        : QObject(parent)
        , m_major(0)
        , m_minor(0) {}

private:
    QString m_uuid;
    qint32 m_major;
    qint32 m_minor;

    friend class Region;
};

#endif // GBEACONREGION_P_H
