#ifndef BEACONMODELIOS_H
#define BEACONMODELIOS_H

#include <QObject>
#include "gbeaconmodel_p.h"

class BeaconModel;

class BeaconModelIosPrivate : public BeaconModelPrivate
{
    Q_OBJECT
public:
    BeaconModelIosPrivate(BeaconModel *q);
    ~BeaconModelIosPrivate();

    void startMonitoringBeaconsInRegion();
    void stopMonitoringBeaconsInRegion();

    void startRangingBeaconsInRegion();
    void stopRangingBeaconsInRegion();

    bool isInProgress() const;

    void init();

private:
    void *m_delegate;
};

#endif // BEACONMODELIOS_H
