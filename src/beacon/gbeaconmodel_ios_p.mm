#include "gbeaconmodel_ios_p.h"
#include "gbeaconmodel_p.h"
#include "gbeaconmodel.h"
#include <QDebug>

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

static NSString *toNSString(const QString &qString)
{
    QByteArray b = qString.toUtf8();
    return qString.isEmpty() ? nil : [[NSString alloc] initWithFormat:@"%s", b.data()];
}

Region *getRegion(CLRegion *region, QList<Region *> regions)
{
    QString identifier = QString::fromNSString([region identifier]);
    QStringList list = identifier.split('.');

    QString uuid = list.at(0);
    qint32 major = list.at(1).toInt();
    qint32 minor = list.at(2).toInt();

    int i = 0;
    int len = regions.count();
    Region *current = 0;

    for (; i < len; i++) {
        current = regions.at(i);
        if (current->uuid() == uuid
                && current->major() == major
                && current->minor() == minor) {
            return current;
        }
    }

    return 0;
}

@interface BeaconModelAppleImpl : NSObject <CLLocationManagerDelegate>

@property (assign, nonatomic) CLLocationManager *locationManager;
@property (assign, nonatomic) NSMutableDictionary *beacons;
@property (assign, nonatomic) NSMutableArray *beaconRegions;
@property (assign, nonatomic) bool inProgress;

@end

@implementation BeaconModelAppleImpl

BeaconModelIosPrivate *m_context;

- (id)initWithContext:(BeaconModelIosPrivate *)context
{
    self = [super init];
    if (self) {
        m_context = context;
        self.inProgress = NO;
    }

    return self;
}

- (void)dealloc
{
    if (self.inProgress)
        [self stopRangingBeaconsInRegion];

    [self releaseBeacons];
    [self.locationManager release];

    [super dealloc];
}

- (BOOL)isInProgress
{
    return self.inProgress;
}

- (void)prepare
{
    if (!self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
    }

    if (self.beaconRegions)
        [self releaseBeacons];

    self.beaconRegions = [[NSMutableArray alloc] init];
}

- (void)releaseBeacons
{
    int i = 0;
    int len = self.beaconRegions.count;

    CLBeaconRegion *region = nil;
    for (; i < len; i++) {
        region = [self.beaconRegions objectAtIndex:i];
        [region release];
    }

    [self.beaconRegions release];
}

- (void)startRangingBeaconsInRegion:(QList<Region *>)regions
{
    if (self.inProgress)
        return;

    qDebug() << "Beacon ranging started with regions:" << regions << "Total:" << regions.count();

    self.inProgress = YES;

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [self.locationManager requestWhenInUseAuthorization];
    }

    int i = 0;
    int len = regions.count();
    Region *region = 0;

    CLBeaconRegion *nativeRegion = nil;
    NSUUID *uuid = nil;
    NSString *identifier = nil;

    for (; i < len; i++) {
        region = regions.at(i);
        uuid = [[NSUUID alloc] initWithUUIDString:toNSString(region->uuid())];
        identifier = [NSString stringWithFormat:@"%@.%d.%d", toNSString(region->uuid()), region->major(), region->minor()];

        if (region->major() > -1) {
            if (region->minor() > -1) {
                nativeRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                       major:region->major()
                                                       minor:region->minor()
                                                       identifier:identifier];
            } else {
                nativeRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                       major:region->major()
                                                       identifier:identifier];
            }
        } else {
            nativeRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                   identifier:identifier];
        }

        nativeRegion.notifyEntryStateOnDisplay = YES;
        [self.beaconRegions addObject:nativeRegion];
        [self.locationManager startMonitoringForRegion:nativeRegion];
        [self.locationManager startRangingBeaconsInRegion:nativeRegion];

        // NSLog(@"Started ranging beacons in region %@", nativeRegion);
    }
}

- (void)stopRangingBeaconsInRegion
{
    if (!self.locationManager || !self.inProgress)
        return;

    for (CLBeaconRegion *region in self.beaconRegions)
        [self.locationManager stopRangingBeaconsInRegion:region];

    self.inProgress = NO;
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    Q_UNUSED(manager);

    int i = 0;
    int len = [beacons count];
    CLBeacon *beacon = nil;
    Beacon *data = 0;

    for (; i < len; i++) {
        beacon = [beacons objectAtIndex:i];
        data = new Beacon();
        data->setUuid(QString::fromNSString(beacon.proximityUUID.UUIDString));
        data->setMajor([beacon.major intValue]);
        data->setMinor([beacon.minor intValue]);
        data->setDistance([beacon accuracy]);
        data->setRssi([beacon rssi]);

        // NSLog(@"Beacon detected %@ %d %d", beacon.proximityUUID.UUIDString, [beacon.major intValue], [beacon.minor intValue]);

        emit m_context->q()->beaconDetectedInRange(data, getRegion(region, m_context->regions()));
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    Q_UNUSED(manager);

    qDebug() << "Enter Region: " << region.identifier;
    emit m_context->q()->regionStateChanged(getRegion(region, m_context->regions()), BeaconModel::RegionEnter);
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    Q_UNUSED(manager);
    Q_UNUSED(region);

    qDebug() << "Exit Region: " << region.identifier;

    emit m_context->q()->regionStateChanged(getRegion(region, m_context->regions()), BeaconModel::RegionExit);
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    qDebug() << "State for region" << state;

    if (state == CLRegionStateInside)
        [manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
    else
        [manager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
}

@end

BeaconModelPrivate *BeaconModelPrivate::create(BeaconModel *q)
{
    return new BeaconModelIosPrivate(q);
}

BeaconModelIosPrivate::BeaconModelIosPrivate(BeaconModel *q)
    : BeaconModelPrivate(q)
    , m_delegate([[BeaconModelAppleImpl alloc] initWithContext:this])
{
}

BeaconModelIosPrivate::~BeaconModelIosPrivate()
{

}

void BeaconModelIosPrivate::startMonitoringBeaconsInRegion()
{
    BeaconModelAppleImpl *delegate = id(m_delegate);
    if ([delegate isInProgress]) {
        [delegate stopRangingBeaconsInRegion];
    }
}

void BeaconModelIosPrivate::stopMonitoringBeaconsInRegion()
{

}

void BeaconModelIosPrivate::startRangingBeaconsInRegion()
{
    BeaconModelAppleImpl *delegate = id(m_delegate);
    [delegate startRangingBeaconsInRegion:m_regions];
}

void BeaconModelIosPrivate::stopRangingBeaconsInRegion()
{
    BeaconModelAppleImpl *delegate = id(m_delegate);
    [delegate stopRangingBeaconsInRegion];
}

bool BeaconModelIosPrivate::isInProgress() const
{
    BeaconModelAppleImpl *delegate = id(m_delegate);
    return [delegate isInProgress] ? true : false;
}

void BeaconModelIosPrivate::init()
{
    BeaconModelAppleImpl *delegate = id(m_delegate);
    [delegate prepare];
}
