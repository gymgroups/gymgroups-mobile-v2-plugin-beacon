#ifndef BEACONMODEL_H
#define BEACONMODEL_H

#include <QObject>
#include <qqml.h>
#include "gbeacon_global.h"
#include <GroupsIncMvc/gmvcmodel.h>

class BeaconModelPrivate;
class RegionPrivate;
class BeaconPrivate;

class Q_BEACON_EXPORT Beacon : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qint32 rssi READ rssi WRITE setRssi NOTIFY rssiChanged)
    Q_PROPERTY(qint32 txPower READ txPower WRITE setTxPower NOTIFY txPowerChanged)
    Q_PROPERTY(QString uuid READ uuid WRITE setUuid NOTIFY uuidChanged)
    Q_PROPERTY(qint32 major READ major WRITE setMajor NOTIFY majorChanged)
    Q_PROPERTY(qint32 minor READ minor WRITE setMinor NOTIFY minorChanged)
    Q_PROPERTY(qreal distance READ distance WRITE setDistance NOTIFY distanceChanged)
public:
    explicit Beacon(QObject *parent = 0);
    ~Beacon();

    void setRssi(qint32 rssi);
    qint32 rssi() const;

    void setTxPower(qint32 txPower);
    qint32 txPower() const;

    void setUuid(const QString &uuid);
    QString uuid() const;

    void setMajor(qint32 major);
    qint32 major() const;

    void setMinor(qint32 minor);
    qint32 minor() const;

    void setDistance(qreal distance);
    qreal distance() const;

Q_SIGNALS:
    void rssiChanged();
    void txPowerChanged();
    void uuidChanged();
    void majorChanged();
    void minorChanged();
    void distanceChanged();

private:
    Q_DECLARE_PRIVATE(Beacon)
    BeaconPrivate *d_ptr;
};

class Q_BEACON_EXPORT Region : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString uuid READ uuid WRITE setUuid NOTIFY uuidChanged)
    Q_PROPERTY(qint32 major READ major WRITE setMajor NOTIFY majorChanged)
    Q_PROPERTY(qint32 minor READ minor WRITE setMinor NOTIFY minorChanged)
public:
    explicit Region(QObject *parent = 0);
    ~Region();

    void setUuid(const QString &uuid);
    QString uuid() const;

    void setMajor(qint32 major);
    qint32 major() const;

    void setMinor(qint32 minor);
    qint32 minor() const;

    bool isValid() const;

Q_SIGNALS:
    void uuidChanged();
    void majorChanged();
    void minorChanged();

private:
    Q_DECLARE_PRIVATE(Region)
    RegionPrivate *d_ptr;
};

class Q_BEACON_EXPORT BeaconModel : public MvcModel
{
    Q_OBJECT
    Q_ENUMS(RegionState)
    Q_ENUMS(Error)
public:
    enum RegionState {
        RegionEnter = 0,
        RegionExit
    };

    enum Error {
        NoError = 0,
        NotReady,
        BluetoothNotEnabled,
        BleNotSupported,
        MissingRegion
    };

    explicit BeaconModel(MvcFacade *parent = 0);

    /* For v2
    int regionState() const;
    void setRegionState(int state);

    void setBackgroundMode(bool backgroundMode);
    bool backgroundMode() const;

    void setBackgroundPowerSaver(bool backgroundPowerSaver);
    bool backgroundPowerSaver() const;

    Q_INVOKABLE void setBackgroundBetweenScanPeriod(qint64 scanPeriod);
    Q_INVOKABLE void setBackgroundScanPeriod(qint64 scanPeriod);
    Q_INVOKABLE void setForegroundBetweenScanPeriod(qint64 scanPeriod);
    Q_INVOKABLE void setForegroundScanPeriod(qint64 scanPeriod);
    Q_INVOKABLE void setSampleExpiration(qint64 milliseconds);

    Q_INVOKABLE bool isValid();
    Q_INVOKABLE void init();*/

    Q_INVOKABLE void appendLayout(const QString &layout);
    Q_INVOKABLE void setLayout(const QString &layout);
    Q_INVOKABLE void clearLayout();

    QList<QString> layouts() const;

    Q_INVOKABLE bool addRegion(const QString &uuid, qint32 major = -1, qint32 minor = -1);
    Q_INVOKABLE bool hasRegion(const QString &uuid, qint32 major = -1, qint32 minor = -1);
    Q_INVOKABLE bool removeRegion(const QString &uuid, qint32 major = -1, qint32 minor = -1);

    Q_INVOKABLE void startMonitoringBeaconsInRegion();
    Q_INVOKABLE void stopMonitoringBeaconsInRegion();

    Q_INVOKABLE void startRangingBeaconsInRegion();
    Q_INVOKABLE void stopRangingBeaconsInRegion();

    Q_INVOKABLE bool isInProgress() const;

    int error() const;
    void setError(int error);

    void init();
    const char *name();
    void apply(const QVariantMap &config);

    static const char *NAME;

Q_SIGNALS:
    void beaconDetectedInRange(Beacon *beacon, Region *region);
    void errorChanged();
    void regionStateChanged(Region *region, RegionState state);

private:
    Q_DECLARE_PRIVATE(BeaconModel)
    BeaconModelPrivate *d_ptr;
};

QML_DECLARE_TYPE(BeaconModel)
QML_DECLARE_TYPE(Beacon)
QML_DECLARE_TYPE(Region)

#endif // BEACONMODEL_H
